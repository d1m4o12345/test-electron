const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(':memory:');

const inputSell = document.getElementById('sell');
const inputBuy = document.getElementById('buy');
const inputBank = document.getElementById('bank');
const dataList = document.getElementById("database");

let rangeData = [];
let orgMass = [];
let rangeDataString;

const getAllRowFromDB = () => {
  return new Promise((resolve, reject) => {
    db.run("CREATE TABLE IF NOT EXISTS ExchangeRates (info TEXT)", {}, function (err) {
      if (err) {
        reject(err);
      }
      db.each(`SELECT rowid AS id, info FROM ExchangeRates`, (err, row) => {
      if (err) {
        reject(err);
      } else {
        var item = document.createElement("li");
        item.textContent = "" + row.id + ": " + row.info;
        dataList.appendChild(item);
      }
      }, (err, n) => {
        if (err) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    });

  });
}

const addRowToDB = (textRow) => {
  return new Promise((resolve, reject) => {
    db.run("CREATE TABLE IF NOT EXISTS ExchangeRates (info TEXT)");
    var stmt = db.prepare(`INSERT INTO ExchangeRates VALUES (?)`);
    stmt.run(textRow);
    stmt.finalize();
    db.each(`SELECT rowid AS id, info FROM ExchangeRates ORDER BY ID DESC LIMIT 1`, (err, row) => {
      if (err) {
        reject(err);
      } else {
        var item = document.createElement("li");
        item.textContent = "" + row.id + ": " + row.info;
        dataList.appendChild(item);
      }
    }, (err, n) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}

const getData = new Promise((resolve, reject) => {
  return fetch('http://resources.finance.ua/ru/public/currency-cash.json', {method : 'GET'})
    .then(res => res.text())
    .catch(err => console.error('что-то пошло не так', err))
    .then(rs => {
      window.rs200 = JSON.parse(rs);
      const { organizations } = JSON.parse(rs);
      organizations.forEach(el => {
        if(el.cityId === '7oiylpmiow8iy1smadi' && el.title === 'ПриватБанк') {
          orgMass.push(el);
        }
      });
      inputBank.value = orgMass[0].title;
      inputBuy.value = orgMass[0].currencies.USD.bid;
      inputSell.value = orgMass[0].currencies.USD.ask;
      rangeData = [];
      const currentDate = new Date();
      const btfdate = `${currentDate.getUTCFullYear()}-${(currentDate.getUTCMonth() + 1 < 10) ? `0${currentDate.getUTCMonth() + 1}` : currentDate.getUTCMonth() + 1}-${(currentDate.getDate() < 10) ? `0${currentDate.getDate()}` : currentDate.getDate()}  ${currentDate.getHours()}:${currentDate.getMinutes()}`;
      rangeData.push(btfdate);
      rangeData.push(orgMass[0].title);
      rangeData.push(orgMass[0].currencies.USD.bid);
      rangeData.push(orgMass[0].currencies.USD.ask);
      rangeDataString = rangeData.join(' ');
      return addRowToDB(rangeDataString)
      .then(() => {
        resolve(true);
      });
    })
    .catch(err => reject(err))
  });

const updateData = () => {
  return fetch('http://resources.finance.ua/ru/public/currency-cash.json', {method : 'GET'})
    .then(res => res.text())
    .catch(err => console.error('что-то пошло не так', err))
    .then(rs => {
      window.rs200 = JSON.parse(rs);
      const { organizations } = JSON.parse(rs);
      organizations.forEach(el => {
        if(el.cityId === '7oiylpmiow8iy1smadi' && el.title === 'ПриватБанк') {
          orgMass.push(el);
        }
      });
      inputBank.value = orgMass[0].title;
      inputBuy.value = orgMass[0].currencies.USD.bid;
      inputSell.value = orgMass[0].currencies.USD.ask;
      rangeData = []; 
      const currentDate = new Date();
      const btfdate = `${currentDate.getUTCFullYear()}-${(currentDate.getUTCMonth() + 1 < 10) ? `0${currentDate.getUTCMonth() + 1}` : currentDate.getUTCMonth() + 1}-${(currentDate.getDate() < 10) ? `0${currentDate.getDate()}` : currentDate.getDate()}  ${currentDate.getHours()}:${currentDate.getMinutes()}`;
      rangeData.push(btfdate);
      rangeData.push(orgMass[0].title);
      rangeData.push(orgMass[0].currencies.USD.bid);
      rangeData.push(orgMass[0].currencies.USD.ask);
      rangeDataString = rangeData.join(' ');
      return addRowToDB(rangeDataString)
        .then(() => {
          return true;
        })
    })
    .catch(err => console.error(err));
}

const init = () => {
  return getAllRowFromDB()
  .catch(err => console.error('База пуста', err))
  .then(() => {
    return getData
  })
}
init();

setInterval(updateData, 1000 * 60 * 10); 
